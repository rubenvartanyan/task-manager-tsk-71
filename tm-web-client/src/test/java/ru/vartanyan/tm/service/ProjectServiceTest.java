package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.vartanyan.tm.configuration.DatabaseConfiguration;
import ru.vartanyan.tm.exception.EmptyIdException;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class ProjectServiceTest {

    @Nullable
    @Autowired
    private ProjectService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private Project project;

    @NotNull
    private static String USER_ID;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        projectService.clear();
        project = projectService.add(USER_ID, new Project("Project"));
    }

    @Test
    @Category(UnitCategory.class)
    public void add() {
        @NotNull final Project projectById = projectService.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        @NotNull final List<Project> projects = projectService.findAll();
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectService.findAll(USER_ID);
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Project> projects = projectService.findAll("test");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findById() {
        @NotNull final Project project = projectService.findById(USER_ID, this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Project project = projectService.findById(USER_ID, "34");
        Assert.assertNull(project);
    }

    @Category(UnitCategory.class)
    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final Project project = projectService.findById(USER_ID, null);
        Assert.assertNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdIncorrectUser() {
        @NotNull final Project project = projectService.findById("test", this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void remove() {
        projectService.removeById(project.getId());
        Assert.assertNull(projectService.findById(project.getId()));
    }

    @Category(UnitCategory.class)
    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        projectService.removeById(null);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByName() {
        @NotNull final Project project = projectService.findByName(USER_ID, "Project");
        Assert.assertNotNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Project project = projectService.findByName(USER_ID, "34");
        Assert.assertNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByNameIncorrectUser() {
        @NotNull final Project project = projectService.findByName("test", this.project.getName());
        Assert.assertNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeById() {
        projectService.removeById(USER_ID, project.getId());
        Assert.assertNull(projectService.findById(project.getId()));
    }

}
