package ru.vartanyan.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.vartanyan.tm.configuration.DatabaseConfiguration;
import ru.vartanyan.tm.configuration.WebApplicationConfiguration;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                WebApplicationConfiguration.class,
                DatabaseConfiguration.class
        }
)
public class TaskEndpointTest {

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/tasks/";

    private final Task task = new Task("test");

    private final Task task2 = new Task("test2");

    @Nullable
    private String USER_ID;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void before() {
        auth();
        deleteAll();
        create(task);
    }

    public void auth() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
    }

    @Test
    @Category(UnitCategory.class)
    public void findTest() {
        Assert.assertEquals(task.getName(), find(task.getId()).getName());
    }

    @SneakyThrows
    @Nullable
    private Task find(String taskId) {
        @NotNull String url = TASK_URL + "find/" + taskId;
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        return objectMapper.readValue(json, Task.class);
    }

    @Test
    @Category(UnitCategory.class)
    public void createTest() {
        create(task);
        @Nullable final Task taskNew = find(task.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(task.getId(), taskNew.getId());
    }

    @SneakyThrows
    @NotNull
    private void create(Task task) {
        @NotNull String url = TASK_URL + "create";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(task);
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void update() {
        final Task updatedTask = find(task.getId());
        updatedTask.setName("updated");
        @NotNull final String url = TASK_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(updatedTask);
        this.mockMvc.perform(
                MockMvcRequestBuilders.put(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        Assert.assertEquals("updated", find(task.getId()).getName());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteTest() {
        delete(task.getId());
        Assert.assertEquals(0, findAll().size());
    }

    @SneakyThrows
    private void delete(String taskId) {
        @NotNull final String url = TASK_URL + "delete/" + taskId;
        this.mockMvc.perform(
                MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTest() {
        Assert.assertEquals(1, findAll().size());
        create(task2);
        Assert.assertEquals(2, findAll().size());
    }

    @SneakyThrows
    @NotNull
    private List<Task> findAll() {
        @NotNull final String url = TASK_URL + "findAll";
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        return Arrays.asList(objectMapper.readValue(json, Task[].class));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void updateAll() {
        create(task2);
        final Task updatedTask = find(task.getId());
        updatedTask.setName("updated1");
        final Task updatedTask2 = find(task2.getId());
        updatedTask2.setName("updated2");
        List<Task> updatedList = new ArrayList<>();
        updatedList.add(updatedTask);
        updatedList.add(updatedTask2);
        @NotNull final String url = TASK_URL + "saveAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(updatedList);
        this.mockMvc.perform(
                MockMvcRequestBuilders.put(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        Assert.assertEquals("updated1", find(task.getId()).getName());
        Assert.assertEquals("updated2", find(task2.getId()).getName());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteAllTest() {
        create(task2);
        Assert.assertEquals(2, findAll().size());
        deleteAll();
        Assert.assertEquals(0, findAll().size());
    }

    @SneakyThrows
    @NotNull
    private void deleteAll() {
        this.mockMvc.perform(
                MockMvcRequestBuilders.delete(TASK_URL + "deleteAll")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

}
