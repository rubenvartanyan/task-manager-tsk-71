package ru.vartanyan.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.EmptyIdException;
import ru.vartanyan.tm.exception.NoSuchTaskException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.repository.ITaskRepository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskService extends AbstractBusinessEntityService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<Task> collection) {
        if (collection == null) return;
        for (Task item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable final Task entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new)).orElse(null);
    }

    @Override
    @SneakyThrows
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Task entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        return repository.findByIndexAndUserId(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByName(@NotNull final String userId, @Nullable final String name) {
        return repository.findByUserIdAndName(userId, name);
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        repository.deleteByUserIdAndName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = Optional.ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(NoSuchTaskException::new);
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description) {

        @NotNull final Task task = Optional.ofNullable(repository.findByIndexAndUserId(userId, index))
                .orElseThrow(NoSuchTaskException::new);
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = Optional.ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(NoSuchTaskException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        @NotNull final Task task = Optional.ofNullable(repository.findByIndexAndUserId(userId, index))
                .orElseThrow(NoSuchTaskException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startByName(@NotNull final String userId, @Nullable final String name) {
        @NotNull final Task task = Optional.ofNullable(repository.findByUserIdAndName(userId, name))
                .orElseThrow(NoSuchTaskException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = Optional.ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(NoSuchTaskException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        @NotNull final Task task = Optional.ofNullable(repository.findByIndexAndUserId(userId, index))
                .orElseThrow(NoSuchTaskException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task finishByName(@NotNull final String userId, @Nullable final String name) {
        @NotNull final Task task = Optional.ofNullable(repository.findByUserIdAndName(userId, name))
                .orElseThrow(NoSuchTaskException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        repository.save(task);
        return task;
    }

    @SneakyThrows
    @Nullable
    public Task add(String user, @Nullable String name, @Nullable String description) {
        @Nullable final Task task = new Task(name, description);
        add(user, task);
        return (task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void addAll(final String userId, @Nullable final Collection<Task> collection) {
        if (collection == null || collection.isEmpty()) return;
        addAll(collection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(final String user, @Nullable final Task entity) {
        if (entity == null) return null;
        entity.setUserId(user);
        @Nullable final Task entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findByUserIdAndId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        repository.deleteByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteByUserIdAndId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final Task entity) {
        if (entity == null) return;
        repository.deleteByUserIdAndId(userId, entity.getId());
    }

}
