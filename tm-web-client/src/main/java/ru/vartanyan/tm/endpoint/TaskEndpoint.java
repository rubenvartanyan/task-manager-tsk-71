package ru.vartanyan.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vartanyan.tm.api.endpoint.ITaskEndpoint;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.service.TaskService;
import ru.vartanyan.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
@RestController
@RequestMapping("/api/tasks")
public class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private TaskService service;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(service.findAll(UserUtil.getUserId()));
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") @WebParam(name = "id") final String id) {
        return service.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Task create(@RequestBody @WebParam(name = "task") final Task task) {
        service.add(UserUtil.getUserId(), task);
        return task;
    }

    @Override
    @WebMethod
    @PostMapping("/createAll")
    public List<Task> createAll(@RequestBody @WebParam(name = "tasks") final List<Task> tasks) {
        service.addAll(UserUtil.getUserId(), tasks);
        return tasks;
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public Task save(@RequestBody @WebParam(name = "task") final Task task) {
        service.add(UserUtil.getUserId(), task);
        return task;
    }

    @Override
    @WebMethod
    @PutMapping("/saveAll")
    public List<Task> saveAll(@RequestBody @WebParam(name = "tasks") final List<Task> tasks) {
        service.addAll(UserUtil.getUserId(), tasks);
        return tasks;
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") @WebParam(name = "id") final String id) {
        service.removeById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        service.clear(UserUtil.getUserId());
    }

}
