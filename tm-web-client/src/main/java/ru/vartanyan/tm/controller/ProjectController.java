package ru.vartanyan.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.AuthorizedUser;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.service.ProjectService;

@Controller
public class ProjectController {

    @Autowired
    private ProjectService service;

    @Secured({"ROLE_USER"})
    @GetMapping("/project/create")
    public String create(@AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user) {
        service.add(user.getUserId(), new Project("New Project" + System.currentTimeMillis()));
        return "redirect:/projects";
    }

    @Secured({"ROLE_USER"})
    @GetMapping("/project/delete/{id}")
    public String delete(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user,
            @PathVariable("id") String id
    ) {
        service.removeById(user.getUserId(), id);
        return "redirect:/projects";
    }

    @Secured({"ROLE_USER"})
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user,
            @PathVariable("id") String id
    ) {
        final Project project = service.findById(user.getUserId(), id);
        return new ModelAndView("project-edit", "project", project);
    }

    @Secured({"ROLE_USER"})
    @PostMapping("/project/edit/{id}")
    public String edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user,
            @ModelAttribute("project") Project project, BindingResult result
    ) {
        service.add(user.getUserId(), project);
        return "redirect:/projects";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

}
