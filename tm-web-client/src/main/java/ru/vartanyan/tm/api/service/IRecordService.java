package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.model.AbstractBusinessEntity;

import java.util.Collection;
import java.util.List;

public interface IRecordService<E extends AbstractBusinessEntity> {

    List<E> findAll();

    E add(final E entity);

    void addAll(final Collection<E> collection);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void remove(final E entity);

}