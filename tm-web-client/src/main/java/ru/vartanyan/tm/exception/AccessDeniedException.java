package ru.vartanyan.tm.exception;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! No such task found...");
    }

}
