<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>
<h1>TASK EDIT</h1>

<form:form action="/task/edit/${task.id}/" method="POST" modelAttribute="task">
    <form:input type="hidden" path="id"/>
    <table width="100%" cellpadding="10" border="1" style="margin-top: 20px">
    <tr>
        <td>NAME:</td>
        <td>DESCRIPTION:</td>
        <td>STATUS:</td>
        <td>START DATE:</td>
        <td>FINISH DATE:</td>
        <td>PROJECT:</td>
    </tr>
<tr>
    <td><form:input type="text" path="name"/></td>

    <td><form:input type="text" path="description"/></td>

    <td>
        <form:select path="status">
            <form:option value="${null}" label="---  // ----"/>
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </td>

    <td><form:input type="date" path="startDate"/></td>

    <td><form:input type="date" path="finishDate"/></td>

    <td>
        <form:select path="projectId">
            <form:option value="${null}" label="---  // ----"/>
            <form:options items="${projects}" itemLabel="name" itemValue="id"/>
        </form:select>
    </td>
</tr>
    </table>

    <button type="submit">SAVE</button>
</form:form>

<jsp:include page="../include/_footer.jsp"/>