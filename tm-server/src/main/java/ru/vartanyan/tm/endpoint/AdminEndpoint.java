package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.api.endpoint.IAdminEndpoint;
import ru.vartanyan.tm.api.service.dto.ISessionService;
import ru.vartanyan.tm.api.service.dto.ITaskService;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @Override
    @WebMethod
    public void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        taskService.deleteAll();
    }

    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        taskService.deleteAll();
    }

    @Override
    @WebMethod
    public List<Session> listSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    )  throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        return sessionService.findAll();
    }

}
