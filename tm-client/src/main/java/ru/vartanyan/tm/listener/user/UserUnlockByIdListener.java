package ru.vartanyan.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractUserListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class UserUnlockByIdListener extends AbstractUserListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "user-unlock-by-id";
    }

    @Override
    public String description() {
        return "Unlock user by Id";
    }

    @Override
    @EventListener(condition = "@userUnlockByIdListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[UNLOCK USER BY ID]");
        System.out.println("[ENTER ID]");
        @NotNull final String id = TerminalUtil.nextLine();
        adminEndpoint.unlockUserById(id, session);
    }

}
