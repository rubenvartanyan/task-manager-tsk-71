package ru.vartanyan.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractAuthListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.AccessDeniedException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class AuthLoginListener extends AbstractAuthListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "login";
    }

    @Override
    public String description() {
        return "Login";
    }

    @Override
    @EventListener(condition = "@authLoginListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        SessionDTO session = sessionEndpoint.openSession(login, password);
        if (session == null) throw new AccessDeniedException();
        bootstrap.setSession(session);
    }

}
